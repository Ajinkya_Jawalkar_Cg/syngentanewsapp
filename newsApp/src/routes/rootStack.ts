import { createStackNavigator } from 'react-navigation-stack'
import HomePage from '../screens/homePage/homePage'
import NewsPage from '../screens/newsPage/newsPage'
import DetailsPage from '../screens/detailsPage/detailsPage'

const RootStack = createStackNavigator(
    {
        HomePage,
        NewsPage,
        DetailsPage,
    }, {
    initialRouteName: 'HomePage',
    defaultNavigationOptions: {
        gestureEnabled: false,
        headerShown: false,
    },
},
)

export default RootStack
