import React, { PureComponent } from 'react';
import { createAppContainer } from 'react-navigation';
import { Provider } from 'react-redux';

import RootStack from './routes/rootStack'
import { Store } from './state/store';
import NavigationManager from './modules/navigation-manager/navigation-manager';
import { NetworkProvider } from './components/network-provider/network-provider';

interface ApplicationProps { }

interface ApplicationState { }

const AppContainer = createAppContainer(RootStack);

export default class App extends PureComponent<
    ApplicationProps,
    ApplicationState
> {
    constructor(props: ApplicationProps) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Provider store={Store}>
                <NetworkProvider />
                <AppContainer
                    ref={(navigatorRef: any) => {
                        NavigationManager.setTopLevelNavigator(navigatorRef);
                    }}
                    onNavigationStateChange={() => {
                        console.disableYellowBox = true;
                    }}
                />
            </Provider>
        );
    }
}
