import axios from 'axios'

export enum HTTP_METHODS {
    GET = 'GET',
}

var parser = require('fast-xml-parser');

class RestClientManager {
    public constructor() { }
    /**
     * 
     * @param params : parameter required for specific request like id;
     * @param defaultParams : default parameter for any request like token;
     */
    private prepareParams(params: any, defaultParams: any) {
        if (defaultParams === null) return params
        for (const elem in params) {
            defaultParams[elem] = params[elem]
        }
        return defaultParams
    }

    private prepareHeaders(headers?: any) {
        if (headers === null) {
            headers = {}
        }
        headers.Accept = 'application/json'
        headers['Content-Type'] = 'application/json'
        return headers
    }

    public getRequest<T>(URL: string, params: any, headers?: any): Promise<T> {
        return new Promise((resolve, reject) => {
            params = this.prepareParams(params, null)
            axios({
                method: HTTP_METHODS.GET,
                url: URL,
                headers: this.prepareHeaders(headers),
                data: params,
            }).then((response) => {
                let respJSON: any = response
                try {
                    if (parser.validate(response.data) === true) { //optional (it'll return an object in case it's not valid)
                        respJSON = parser.parse(response.data, true);
                    }
                } catch (ex) { }
                resolve(respJSON)
            }).catch((error) => {
                reject(error)
            });
        })
    }
}

const restClientManager = new RestClientManager()
export default restClientManager