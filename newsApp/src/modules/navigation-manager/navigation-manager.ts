import { any } from 'prop-types'
import { NavigationActions, StackActions } from 'react-navigation'

const NavigationManager = {

    navigator: any,

    setTopLevelNavigator(navigatorRef: any) {
        this.navigator = navigatorRef
    },

    navigate(routeName: any, params?: any) {
        const nav: any = this.navigator
        nav.dispatch(
            NavigationActions.navigate({
                routeName,
                params,
            }),
        )
    },

    navigateAndClear(routeName: any, params?: any) {
        const nav: any = this.navigator
        nav.dispatch(StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName,
                    params,
                }),
            ],
        }),
        )
    },

    goBack() {
        const nav: any = this.navigator
        nav.dispatch(
            NavigationActions.back(),
        )
    },

}

export default NavigationManager