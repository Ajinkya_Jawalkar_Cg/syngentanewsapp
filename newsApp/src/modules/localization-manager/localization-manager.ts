const BaseLocalization = {
    newsPageTitle: 'News Channel',
    detailsPageTitle: 'Details',
    nextButton: 'Next',
    gotoDetails: 'Goto Details',
    detailsLabel: 'Details page',
    genericLoading: 'Loading...',

    set(key: string, value: string): void {
        const loc: any = BaseLocalization
        loc[key] = value
    },
}

export default BaseLocalization
