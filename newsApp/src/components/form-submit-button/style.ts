import { StyleSheet } from 'react-native';
import { theme } from '../../styles/theme';

export const style = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: 'transparent'
    },
    wrapper: {
        flexGrow: 1,
        height: theme.dimensions.heights.buttons,
        overflow: 'hidden',
    },
    button: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: theme.colors.other.colors.success,
    },
    outline: {
        backgroundColor: theme.colors.primary._00,
        borderWidth: theme.dimensions.borderWidths.buttons,
        borderColor: theme.colors.other.colors.success,
    },
    text: {
        color: theme.colors.primary._00,
        textTransform: 'uppercase',
    },
    textOutline: {
        color: theme.colors.other.colors.success,
    },
    disabled: {
        opacity: theme.decorations.opacity.disabledButtons,
    },
    hidden: {
        display: 'none',
    },
});
