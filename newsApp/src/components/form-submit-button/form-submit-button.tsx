
import React, { Fragment, PureComponent } from 'react';
import { ActivityIndicator, Animated, EmitterSubscription, Keyboard, Text, TouchableOpacity } from 'react-native';
import { theme } from '../../styles/theme';

import { style } from './style';


interface FormSubmitButtonProps {
    text: string;
    onPress: () => void;
    disabled?: boolean;
    loading?: boolean;
    outline?: boolean;
    accessibilityLabel?: string;
}

interface FormSubmitButtonState {
    borderRadius: Animated.Value,
    wrapperPadding: Animated.Value
}

export default class FormSubmitButton extends PureComponent<FormSubmitButtonProps, FormSubmitButtonState> {

    keyboardDidShowListener?: EmitterSubscription;
    keyboardDidHideListener?: EmitterSubscription;

    constructor(props: FormSubmitButtonProps) {
        super(props);
        this.state = {
            borderRadius: new Animated.Value(theme.dimensions.borderRadiuses.buttons),
            wrapperPadding: new Animated.Value(theme.metrics.paddings.containers)
        }
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this._keyboardDidShow,
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this._keyboardDidHide,
        );
    }

    componentWillUnmount() {
        this.keyboardDidShowListener?.remove();
        this.keyboardDidHideListener?.remove();
    }

    _keyboardDidShow = () => {
        Animated.timing(
            this.state.borderRadius,
            {
                toValue: 0,
                duration: 400,
                useNativeDriver: false,
            },
        ).start();
        Animated.timing(
            this.state.wrapperPadding,
            {
                toValue: 0,
                duration: 200,
                useNativeDriver: false,
            },
        ).start();
    };

    _keyboardDidHide = () => {
        Animated.timing(
            this.state.borderRadius,
            {
                toValue: theme.dimensions.borderRadiuses.buttons,
                duration: 400,
                useNativeDriver: false,
            },
        ).start();
        Animated.timing(
            this.state.wrapperPadding,
            {
                toValue: theme.metrics.paddings.containers,
                duration: 200,
                useNativeDriver: false,
            },
        ).start();
    };

    getAccessibilityLabel = () => !!this.props.accessibilityLabel && { accessibilityLabel: this.props.accessibilityLabel }

    render() {
        return (
            <Animated.View style={[style.container, { padding: this.state.wrapperPadding }]}>
                <Animated.View style={[style.wrapper, { borderRadius: this.state.borderRadius }]}>
                    <TouchableOpacity
                        onPress={this.props.onPress}
                        style={[style.button, this.props.outline && style.outline, this.props.disabled && style.disabled]}
                        disabled={this.props.disabled}
                        {...this.getAccessibilityLabel()}
                    >
                        <Fragment>
                            {!this.props.loading && (
                                <Text style={[style.text, this.props.outline && style.textOutline, this.props.loading && style.hidden]}>{this.props.text}</Text>
                            )}
                            {this.props.loading && (
                                <ActivityIndicator style={!this.props.loading && style.hidden} color={theme.colors.primary._00} />
                            )}

                        </Fragment>
                    </TouchableOpacity>
                </Animated.View>
            </Animated.View>
        );
    }
}
