import React, { PureComponent } from 'react'
import { Text, TouchableHighlight, View } from 'react-native'
import Svg, { Path } from 'react-native-svg'
import { connect } from 'react-redux'
import { State } from '../../state/state'
import { theme } from '../../styles/theme'

import CustomIcon from '../custom-icon/custom-icon'
import { style } from './style'

interface CustomTopNavProps {
    title?: string,

    logo?: boolean,

    hiddenAmounts?: boolean,

    transparent?: boolean,

    // LEFT ICONS
    chatBotLeft?: boolean,
    onPressChatBotLeft?: () => void
    close?: boolean,
    onPressClose?: () => void

    back?: boolean,
    onPressBack?: () => void

    changeProfile?: boolean,
    onPressChangeProfile?: () => void

    logout?: boolean
    onPressLogout?: () => void

    // RIGHT ICONS
    notifications?: boolean,
    hasUnreadNotifications?: boolean,
    onPressNotifications?: () => void

    chatBotRight?: boolean,
    onPressChatBotRight?: () => void

    amountController?: boolean,
    onPressAmountController?: () => void

    addContacts?: boolean,
    onPressAddContact?: () => void

    disableGradientFormPMI?: boolean
}

interface CustomTopNavState {
}

class CustomTopNav extends PureComponent<CustomTopNavProps, CustomTopNavState> {
    constructor(props: CustomTopNavProps) {
        super(props)
        this.state = {}
    }

    private onPressChatbot = () => { }

    private onPressChangeProfile = () => { }

    private onPressAmountController = () => {
        if (this.props.onPressAmountController) {
            this.props.onPressAmountController()
        }
    }

    onPressLogout = () => {
        if (this.props.onPressLogout) this.props.onPressLogout()
    }

    render() {
        return (
            <View style={style.container}>
                <View style={style.wrapper}>
                    {this.props.logo && (
                        <View style={style.logoWrapper}>
                            <Svg style={style.logo}>
                                <Path d='M19.1222 6.50621L21.1095 11.8349L22.0696 14.2903L25.8411 12.3909L24.707 9.72655L21.4689 2.69629H16.9482L11.152 14.2903L10.7891 15.0776C12.794 14.7791 15.6586 14.2417 16.5324 12.3909L19.1222 6.50621Z' fill='white' />
                                <Path d='M27.0361 19.7458L29.0515 19.7533L26.6337 14.2903L22.8383 16.2929L23.3258 17.5293L23.8825 18.9584C24.1573 19.6973 24.8021 19.7458 25.4646 19.7383L27.0361 19.7458Z' fill='white' />
                                <Path d='M29.8513 21.6714H0V24.388H29.8513V21.6714Z' fill='white' />
                                <Path d='M32.0503 0.890137L30.8523 1.56182L26.6064 3.92763C26.6064 3.92763 25.1477 4.7635 24.2104 4.97246L24.2175 4.98739L24.2104 4.97993C24.4677 5.83072 24.7319 6.38299 24.9751 6.81959C25.0455 6.95019 25.1512 7.04721 25.271 7.12184C25.4261 7.22633 25.6128 7.2823 25.7996 7.28976C25.9863 7.29723 26.1766 7.25245 26.3528 7.16289L32.0538 3.91643L32.0503 0.890137Z' fill='white' />
                                <Path d='M24.7317 9.74886L21.1307 11.8609C16.6241 14.4655 10.7539 16.9881 5.67294 16.5366C3.52711 16.3463 2.23398 15.4917 2.3573 14.0402C2.49824 12.3908 3.85128 10.6482 6.22262 8.90553C8.03723 7.57336 9.62635 6.85317 10.4614 6.53972C11.5537 6.13298 12.4875 5.9464 13.4917 5.89789C13.7348 5.89789 13.9568 5.92028 14.1294 5.97252C14.1259 5.94267 14.1188 5.91282 14.1153 5.88297C13.6573 2.79324 12.8927 2.37903 11.6947 2.42008C8.32969 2.53576 4.10497 4.99859 1.86401 7.84576C0.662484 9.3757 0.0493895 11.2116 0.0212012 12.5214C-0.0210812 14.5253 0.732954 17.253 3.03734 18.6449C5.60247 20.1935 9.27752 20.089 12.6143 19.2905C16.2013 18.4322 19.3301 16.7903 22.4097 15.141L26.1482 13.0886L32.0642 9.49138L32.0889 5.58444L24.7317 9.74886Z' fill='white' />
                            </Svg>
                        </View>
                    )}
                    <View style={style.leftNav}>
                        {this.props.chatBotLeft && (
                            <TouchableHighlight onPress={this.onPressChatbot} underlayColor='transparent' hitSlop={theme.dimensions.hitSlop} >
                                <Text style={style.iconsWrapper}><CustomIcon name='chat-bot' style={style.iconsLeft} /></Text>
                            </TouchableHighlight>
                        )}
                        {this.props.logout && (
                            <TouchableHighlight onPress={this.onPressLogout} underlayColor='transparent' hitSlop={theme.dimensions.hitSlop}>
                                <Text style={style.iconsWrapper}><CustomIcon name='log-out' style={style.iconsLeft} /></Text>
                            </TouchableHighlight>
                        )}


                        {this.props.close && (
                            <TouchableHighlight onPress={this.props.onPressClose} underlayColor='transparent' hitSlop={theme.dimensions.hitSlop}>
                                <Text style={style.iconsWrapper}><CustomIcon name='x' style={style.iconsLeft} /></Text>
                            </TouchableHighlight>
                        )}

                        {this.props.back && (
                            <TouchableHighlight onPress={this.props.onPressBack} underlayColor='transparent' hitSlop={theme.dimensions.hitSlop}>
                                <Text style={style.iconsWrapper}><CustomIcon name='arrow-left' style={style.iconsLeft} /></Text>
                            </TouchableHighlight>
                        )}
                    </View>
                    <View style={style.titleContainer}>
                        {!this.props.logo && (
                            <Text style={style.title} numberOfLines={1} ellipsizeMode={'tail'}>{this.props.title}</Text>
                        )}
                    </View>
                    <View style={style.rightNav}>
                        {this.props.chatBotRight && (
                            <TouchableHighlight onPress={this.props.onPressChatBotRight} underlayColor='transparent' hitSlop={theme.dimensions.hitSlop}>
                                <Text style={style.iconsWrapper}><CustomIcon name='chat-bot' style={style.iconsRight} /></Text>
                            </TouchableHighlight>
                        )}
                        {this.props.amountController && (
                            <TouchableHighlight onPress={this.onPressAmountController} underlayColor='transparent' hitSlop={theme.dimensions.hitSlop}>
                                <Text style={style.iconsWrapper}><CustomIcon name={!this.props.hiddenAmounts ? 'eye' : 'eye-off'} style={style.iconsRight} /></Text>
                            </TouchableHighlight>
                        )}
                        {this.props.addContacts && (
                            <TouchableHighlight onPress={this.props.onPressAddContact} underlayColor='transparent' hitSlop={theme.dimensions.hitSlop}>
                                <Text style={style.iconsWrapper}><CustomIcon name='user-plus' style={style.iconsRight} /></Text>
                            </TouchableHighlight>
                        )}
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state: State) => {
    return {

    }
}

const mapDispatchToProps = (dispatch: any): object => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(React.memo(CustomTopNav))