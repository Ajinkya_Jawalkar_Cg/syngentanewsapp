import { StyleSheet } from 'react-native'

import { getStatusBarHeight } from 'react-native-status-bar-height'
import { theme } from '../../styles/theme'

const statusBarHeight = getStatusBarHeight()

export const style = StyleSheet.create({
    container: {
        width: '100%',
        height: theme.dimensions.heights.topBars,
        backgroundColor: theme.colors.primary._200,
    },
    wrapper: {
        flexGrow: 1,
        flexShrink: 1,
        flexBasis: 0,
        position: 'relative',
        flexDirection: 'row',
        // paddingTop: statusBarHeight,
        paddingHorizontal: theme.metrics.paddings.containers,
    },
    logoWrapper: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        // top: statusBarHeight,
        left: theme.metrics.paddings.containers,
        right: 0,
    },
    logo: {
        width: 33,
        height: 25,
    },
    titleContainer: {
        flexGrow: 1,
        flexShrink: 1,
        justifyContent: 'center',
    },
    title: {
        justifyContent: 'center',
        textAlignVertical: 'center',
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold'
    },
    leftNav: {
        height: theme.dimensions.heights.topBars,
        flexDirection: 'row',
        alignItems: 'center',
    },
    rightNav: {
        flexGrow: 1,
        height: theme.dimensions.heights.topBars,
        flexDirection: 'row-reverse',
        alignItems: 'center',
    },
    iconsWrapper: {
        width: theme.materialIcons.sizes.primary + theme.metrics.paddings.topNavItems,
        height: theme.materialIcons.sizes.primary,
    },
    icons: {
        width: '100%',
        height: theme.dimensions.heights.topBars,
        fontSize: theme.materialIcons.sizes.primary,
        color: 'white',
    },
    iconsLeft: {
        width: '100%',
        height: theme.dimensions.heights.topBars,
        fontSize: theme.materialIcons.sizes.primary,
        color: 'white',
        textAlign: 'left',
    },
    iconsRight: {
        width: '100%',
        height: theme.dimensions.heights.topBars,
        fontSize: theme.materialIcons.sizes.primary,
        color: 'white',
        textAlign: 'right',
    },
    notificationStyle: {
        position: 'absolute',
        top: 0,
        right: 2,
        width: 8,
        height: 8,
        backgroundColor: 'red',
        borderRadius: 8,
    },
})
