import { useNetInfo } from '@react-native-community/netinfo';
import React, { createContext, useContext, useEffect, useState } from 'react';
import { Alert } from 'react-native';

export const NetworkContext = createContext(false);

export const useNetworkContext = () => useContext(NetworkContext);

export const NetworkProvider = (props: any) => {
    const netInfo = useNetInfo();

    const [hasConnection, setHasConnection] = useState(false);

    useEffect(() => {
        if (netInfo.isConnected === false) Alert.alert('Network Error');
        setHasConnection(netInfo.isConnected == null ? false : true);
    }, [netInfo]);

    return () => {
        <NetworkContext.Provider value={{ hasConnection }}>
            {props.children}
        </NetworkContext.Provider>;
    };
};
