import { StyleSheet } from 'react-native';
import { theme } from '../../styles/theme';


export const style = StyleSheet.create({
    container: {
        flexGrow: 1,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        position: 'absolute',
        zIndex: 100,
        elevation: 101,
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center'
    },
    indicatorContainer: {
        width: 96,
        height: 96,
        backgroundColor: theme.colors.other.grayScale._00,
        opacity: .8,
        borderRadius: theme.dimensions.borderRadiuses.containers,
        alignItems: 'center',
        justifyContent: 'center',
    }
});