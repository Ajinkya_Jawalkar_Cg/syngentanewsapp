import React, { Fragment, PureComponent } from 'react';
import { ActivityIndicator, View } from 'react-native';
import BaseLocalization from '../../modules/localization-manager/localization-manager';
import { theme } from '../../styles/theme';
import { style } from './style'

interface FullScreenLoaderProps {
    isLoading: boolean;
    showSpinner?: boolean;
}

interface FullScreenLoaderState {
}

export default class FullScreenLoader extends PureComponent<FullScreenLoaderProps, FullScreenLoaderState> {

    constructor(props: FullScreenLoaderProps) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <Fragment>
                {this.props.isLoading && (
                    <View style={style.container} importantForAccessibility={'no-hide-descendants'} accessibilityLabel={BaseLocalization.genericLoading}>
                        {this.props.showSpinner && (
                            <View style={style.indicatorContainer}>
                                <ActivityIndicator size={'large'} color={theme.colors.primary._200} />
                            </View>
                        )}
                    </View>
                )}
            </Fragment>
        );
    }
}
