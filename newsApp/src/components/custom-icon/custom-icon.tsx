import React from 'react'
import Icon from 'react-native-vector-icons/Feather'

interface CustomIconProps {
    name: string
    color?: string
    size?: number
    style?: any
}
interface CustomIconState { }
class CustomIcon extends React.Component<CustomIconProps, CustomIconState> {
    constructor(props: CustomIconProps) {
        super(props)
    }

    render() {
        return (
            <Icon
                name={this.props.name}
                size={this.props.size}
                color={this.props.color}
                style={this.props.style}
            />
        )
    }
}
export default CustomIcon
