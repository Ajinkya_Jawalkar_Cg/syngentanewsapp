export const data = [
    {
        title: "What is Lorem Ipsum?",
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",
        imgUrl: "https://picsum.photos/id/11/200/300"
    },
    {
        title: "Why do we use it?",
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",
        imgUrl: "https://picsum.photos/id/10/200/300"
    },
    {
        title: "Where can I get some?",
        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",
        imgUrl: "https://picsum.photos/id/12/200/300"
    }
]