
import React from 'react'
import { Image, Text, View } from "react-native"
import { style } from './style'



const CarouselCardItem = ({ item, index }) => {
    return (
        <View style={style.container} key={index}>
            <Image
                source={{ uri: item.imgUrl }}
                style={style.image}
            />
            <Text style={style.header}>{item.title}</Text>
            <Text style={style.body}>{item.body}</Text>
        </View>
    )
}

export default CarouselCardItem