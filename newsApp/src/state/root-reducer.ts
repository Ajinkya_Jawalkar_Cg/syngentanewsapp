/** @format */
import { Action, combineReducers } from 'redux'
import appStateReducer from './appState/reducer'
import { State } from './state'

const AppReducer = combineReducers(
    {
        appState: appStateReducer,
    },
)

const RootReducer = (state: State, action: Action) => {
    return AppReducer(state, action)
}

export default RootReducer