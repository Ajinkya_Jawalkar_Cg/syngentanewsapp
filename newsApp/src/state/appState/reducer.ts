
import { Action } from '../action-type'
import { AppStateActionType } from './action'
import { defaultAppState, IAppState } from './state'

const appStateReducer = (state: IAppState | undefined, action: Action): IAppState => {
    if (!state) {
        return defaultAppState()
    }

    if (action.type === AppStateActionType.SetCurrentPageName) {
        return { ...state, currentPageName: action.data };
    }

    if (action.type === AppStateActionType.IsLoading) {
        return { ...state, isLoading: action.data }
    }

    if (action.type === AppStateActionType.SetApiData) {
        return { ...state, apiData: action.data }
    }

    if (action.type === AppStateActionType.SetSelectedItem) {
        return { ...state, selectedItem: action.data }
    }

    return state
}

export default appStateReducer
