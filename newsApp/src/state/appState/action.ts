import { Alert } from 'react-native';

import NavigationManager from '../../modules/navigation-manager/navigation-manager';
import restClientManager from '../../modules/rest-client-manager/rest-client-manager';
import { ServerManager } from '../../modules/server-manager/server-manager';


export enum AppStateActionType {
    IsLoading = 'IS_LOADING',
    SetApiData = 'SET_API_DATA',
    SetCurrentPageName = 'SET_CURRENT_PAGE_NAME',
    SetSelectedItem = 'SET_SELECTED_ITEM'
}

export function setCurrentPageName(pageName: string) {
    return (dispatch: any) => {
        dispatch({
            type: AppStateActionType.SetCurrentPageName,
            data: pageName,
        });
    };
}

export function goToPage(pageName: string, params?: any, clearHistory?: boolean) {
    if (!clearHistory) {
        return function (dispatch: any) {
            NavigationManager.navigate(pageName, params);

            if (params && params.screen) {
                dispatch(setCurrentPageName(params.screen));
            } else {
                dispatch(setCurrentPageName(pageName));
            }
        };
    } else {
        return function (dispatch: any) {
            NavigationManager.navigateAndClear(pageName, params);
            if (params && params.screen) {
                dispatch(setCurrentPageName(params.screen));
            } else {
                dispatch(setCurrentPageName(pageName));
            }
        };
    }
}

export function isLoading(loder: boolean) {
    return (dispatch: any) => {
        dispatch({
            type: AppStateActionType.IsLoading,
            data: loder,
        })
    }
}


export function setApiData(apiData: any) {
    return (dispatch: any) => {
        dispatch({
            type: AppStateActionType.SetApiData,
            data: apiData,
        })
    }
}

export function callRssFeed() {
    return (dispatch: any, getState: any) => {
        dispatch(isLoading(true))
        restClientManager.getRequest(ServerManager.getApiData(), {}, {}).then((res: any) => {
            dispatch(setApiData(res))
            dispatch(isLoading(false))
        })
            .catch(err => {
                Alert.alert(err.message)
                console.log(err)
                dispatch(isLoading(false))
            })

    }
}


export function setSelectedItem(selectedItem: any) {
    return (dispatch: any) => {
        dispatch({
            type: AppStateActionType.SetSelectedItem,
            data: selectedItem,
        })
    }
}
