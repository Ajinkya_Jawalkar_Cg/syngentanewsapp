import { State } from '../state'
import { IAppState } from './state'

export function getAppState(state: State): IAppState {
    return state.appState
}
export function getCurrentPageName(state: State) {
    return getAppState(state).currentPageName;
}

export function getIsLoading(state: State) {
    return getAppState(state).isLoading
}

export function getApiData(state: State) {
    return getAppState(state).apiData
}

export function getSelectedItem(state: State) {
    return getAppState(state).selectedItem
}


