export interface IAppState {
    currentPageName?: string;
    isLoading: boolean,
    apiData: any,
    selectedItem: any
}

export function defaultAppState(): IAppState {
    return {
        currentPageName: undefined,
        isLoading: false,
        apiData: {},
        selectedItem: {}
    }
}
