import AsyncStorage from '@react-native-community/async-storage'
import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { persistReducer, persistStore } from 'redux-persist'
import thunkMiddleware from 'redux-thunk'
import RootReducer from './root-reducer'

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['appState'],
}

const persistedReducer = persistReducer(persistConfig, RootReducer)

export const Store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(thunkMiddleware)))
export const Persistor = persistStore(Store)
