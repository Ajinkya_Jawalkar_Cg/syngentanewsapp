import React, { PureComponent } from 'react';
import { WebView } from 'react-native-webview';
import { connect } from 'react-redux';
import CustomTopNav from '../../components/custom-top-nav/custom-top-nav';
import BaseLocalization from '../../modules/localization-manager/localization-manager';
import NavigationManager from '../../modules/navigation-manager/navigation-manager';
import { getSelectedItem } from '../../state/appState/selector';
import { State } from '../../state/state';
import { style } from './style';

interface DetailsPageProps {
    selectedItem: any
}

interface DetailsPageState { }

class DetailsPage extends PureComponent<DetailsPageProps, DetailsPageState> {
    constructor(props: DetailsPageProps) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <>
                <CustomTopNav
                    title={BaseLocalization.detailsPageTitle + " : " + this.props.selectedItem.description}
                    onPressBack={() => { NavigationManager.goBack() }}
                    back
                />
                <WebView style={style.container} source={{ uri: "" + this.props.selectedItem.link }} />
            </>
        );
    }
}

const mapStateToProps = (state: State) => ({
    selectedItem: getSelectedItem(state),
});

const mapDispatchToProps = (dispatch: any, ownProps: any) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPage);
