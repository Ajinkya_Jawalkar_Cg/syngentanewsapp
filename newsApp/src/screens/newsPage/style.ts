import { StyleSheet } from 'react-native'
import { theme, windowWidth } from '../../styles/theme'

export const style = StyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 24
    },
    listItem: {
        margin: 10,
        padding: 10,
        backgroundColor: "#FFF",
        // width: "80%",
        flex: 1,
        flexGrow: 1,
        alignSelf: "center",
        flexDirection: "row",
        borderRadius: 5
    },
    touchableItem: {
        flexDirection: 'row',
        flex: 1,
        borderWidth: 1,
        borderRadius: 8,
        padding: 10,
        margin: 10
    },
    newsContainer: {
        justifyContent: 'center',
        marginLeft: 5
    },
    newsImage: {
        width: windowWidth * 0.75,
        height: 200,
        alignContent: 'center'
    },

    newsTitle: {
        paddingBottom: 4,
        textAlign: 'justify',
        fontSize: 18,
        fontWeight: 'bold',
        color: theme.colors.secondary._600
    },
    date: { justifyContent: 'center', backgroundColor: 'red', color: 'white', fontWeight: 'bold', fontSize: 18, position: 'absolute' }
})