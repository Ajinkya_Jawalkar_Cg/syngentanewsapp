import React, { PureComponent } from 'react';
import { FlatList, Image, RefreshControl, SafeAreaView, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import CustomTopNav from '../../components/custom-top-nav/custom-top-nav';
import FullScreenLoader from '../../components/full-screen-loader/full-screen-loader';
import BaseLocalization from '../../modules/localization-manager/localization-manager';
import NavigationManager from '../../modules/navigation-manager/navigation-manager';
import { callRssFeed, setSelectedItem } from '../../state/appState/action';
import { getApiData, getIsLoading } from '../../state/appState/selector';
import { State } from '../../state/state';
import { style } from './style';


interface NewsPageProps {
    apiData: any;
    isLoading: boolean;
    callRssFeed: () => void;
    setSelectedItem: (item: any) => void;
}

interface NewsPageState {
}

class NewsPage extends PureComponent<NewsPageProps, NewsPageState> {
    constructor(props: NewsPageProps) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.props.callRssFeed();
    }

    _onPress(item: any) {
        this.props.setSelectedItem(item)
        NavigationManager.navigate('DetailsPage');
    }

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                style={style.touchableItem}
                onPress={() => this._onPress(item)}
            >
                <View style={style.newsContainer}>
                    <Text style={style.newsTitle}>{item.title}</Text>

                    <Image
                        style={style.newsImage}
                        source={{ uri: 'https://picsum.photos/id/1/200/300' }}
                        resizeMethod={'resize'}
                    />
                    <Text style={style.date}>{item.pubDate.substr(0, 16)}</Text>
                    <Text>{item.description}</Text>
                </View>
            </TouchableOpacity>
        );
    };


    render() {
        return (
            <>
                <CustomTopNav
                    title={BaseLocalization.newsPageTitle}
                    onPressBack={() => { NavigationManager.goBack() }}
                    back
                />
                <FullScreenLoader isLoading={this.props.isLoading} showSpinner />
                {!this.props.isLoading && this.props.apiData.rss && this.props.apiData.rss.channel && < SafeAreaView style={style.container}>
                    <View>
                        <Text style={style.newsTitle}>{this.props.apiData.rss.channel.title}</Text>
                    </View>
                    <FlatList
                        data={this.props.apiData.rss.channel.item ? this.props.apiData.rss.channel.item : []}
                        renderItem={this.renderItem}
                        keyExtractor={item => item.url}
                        refreshControl={
                            <RefreshControl
                                //refresh control used for the Pull to Refresh
                                refreshing={this.props.isLoading}
                                onRefresh={() => this.props.callRssFeed()}
                            />
                        }
                    />
                </SafeAreaView>}
            </>
        );
    }
}

const mapStateToProps = (state: State) => ({
    apiData: getApiData(state),
    isLoading: getIsLoading(state),
});

const mapDispatchToProps = (dispatch: any, ownProps: any) => ({
    callRssFeed: () => {
        dispatch(callRssFeed());
    },

    setSelectedItem: (item: any) => {
        dispatch(setSelectedItem(item));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsPage);
