import React, { PureComponent } from 'react';
import { SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import CarouselCards from '../../components/carousel-cards/carausel-cards';
import FormSubmitButton from '../../components/form-submit-button/form-submit-button';
import BaseLocalization from '../../modules/localization-manager/localization-manager';
import NavigationManager from '../../modules/navigation-manager/navigation-manager';
import { State } from '../../state/state';
import { style } from './style';

interface HomePageProps {
}

interface HomePageState { }

class HomePage extends PureComponent<HomePageProps, HomePageState> {
    constructor(props: HomePageProps) {
        super(props);
        this.state = {};
    }

    onPressToNavigate = () => {
        NavigationManager.navigate('NewsPage');
    };

    render() {
        return (
            <SafeAreaView style={style.container}>
                <CarouselCards />
                <FormSubmitButton
                    onPress={this.onPressToNavigate}
                    text={BaseLocalization.nextButton}
                />
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: State) => ({});

const mapDispatchToProps = (dispatch: any, ownProps: any) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);