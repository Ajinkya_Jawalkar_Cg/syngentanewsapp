import { Dimensions } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export const statusBarHeight = getStatusBarHeight();
export const windowWidth = Dimensions.get('window').width;
export const windowHeight = Dimensions.get('window').height;

export const theme = {
    colors: {
        primary: {
            _100: '#004D60',
            _200: '#006A7C',
            _00: '#FFFFFF',
        },
        secondary: {
            _500: '#009787',

            _50: '#E0F3F1',
            _100: '#B3E0DB',
            _200: '#80CBC3',
            _300: '#4DB6AB',
            _400: '#26A799',
            _600: '#008F7F',
            _700: '#008474',
            _800: '#007A6A',
            _900: '#006957',
            _1000: '#EFEFEF',
        },
        backgrounds: {
            headers: '#007D8F',
            generalAppUI: '#FFFFFF',
            cards: '#FAFAFA',
            surfaceMenu: '#FAFAFA',
            drawers: '#FFFFFF',
            bottomSheets: '#FAFAFA',
            snackbarSurface: '#202020',
            sideSheets: '#FFFFFF',
            dialog: '#FFFFFF',
            accordionBody: '#F6FBFB',
            accordionBody_50: '#DCEEF4',
            transparent: 'transparent',
            linearGradientLight: '#4ACA9B',
            linearGradientDark: '#007D8F',
            linearGradientPMI: '#009083',
            accordionF24: '#DCEEF4',
        },
        other: {
            grayScale: {
                _500: '#9E9E9E',

                _00: '#FFFFFF',
                _50: '#FAFAFA',
                _100: '#F5F5F5',
                _200: '#EEEEEE',
                _300: '#E0E0E0',
                _400: '#B3B3B3',
                _600: '#757575',
                _700: '#616161',
                _800: '#424242',
                _900: '#212121',
            },
            colors: {
                danger: '#E36363',
                warning: '#F2994A',
                success: '#4ACA9B',
                lightSuccess: '#7FF3D0',
                body3: '#005B63',
                body2: '#8A8A8A',
                wrong: '#AC1A2F',
                body4: '#006E75'
            },
            chart: {
                green: '#09A979',
                blue: '#00696B',
                greenLight: '#43BC9C',
                greenDark: '#089168',
                greenLightBoder: '#46CE95',
            },
            buttons: {
                darkGreen: '#269CA2',
            },
            gradients: {
                limitsGreen: {
                    left: '#039200',
                    right: '#00D6BC',
                },
                limitsLightBlue: {
                    left: '#019489',
                    right: '#00BCD6',
                },
                limitsBlue: {
                    left: '#3C4CDC',
                    right: '#7CAEEA',
                },
                limitsPurple: {
                    left: '#DC3C76',
                    right: '#A67CEA',
                },
            },
        },
    },
    materialIcons: {
        sizes: {
            primary: 24,
            checkbox: 24,
            small: 16,
            row: 30,
        },
    },
    dimensions: {
        hitSlop: { top: 10, bottom: 10, left: 10, right: 10 },
        widths: {
            dots: 10,
            radio: 20,
            checkBox: 24,
            fabs: 40,
            marker: 50,
        },
        heights: {
            dots: 10,
            radio: 20,
            checkBox: 24,
            buttons: 44,
            topBars: 56,
            bottomBars: 56,
            formFields: 40,
            formTwoLinesFields: 64,
            headers: 64,
            listsItems: 80,
            customItemsList: 50,
            tabMenus: 48,
            backdropMin: 48,
            backdropAtmMinHeightBasic: 155,
            backdropAtmMinHeightFull: 205,
            tutorialTop: 18,
            infoTouchableIcon: 30,
            cardContactImportHeight: 118,
            amountFieldBold: 90,
        },
        borderWidths: {
            buttons: 1,
            formFields: 1.2,
            radio: 2,
            listsItems: 1,
        },
        borderRadiuses: {
            containers: 8,
            buttons: 22,
            formFields: 4,
            customItemsList: 4,
        },
    },
    metrics: {
        paddings: {
            bodyHorizontal: 0,
            bodyVertical: 0,
            wrapperHorizontal: 16,
            wrapperVertical: 16,
            formFieldsHorizontal: 16,
            formFieldsVertical: 8,
            containers: 16,
            icons: 16,
            search: 24,
            topNavItems: 24,
        },
        margins: {
            containers: 8,
            formElements: 16,
            customItemsList: 5,
            icons: 16,
            dotMargin: 2,
        },
    },
    decorations: {
        opacity: {
            disabledButtons: 0.5,
        },
        shadows: {
            containers: {
                //iOS
                shadowColor: '#000',
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.2,
                shadowRadius: 1.41,
                //Android
                elevation: 2,
            },
            backdrop: {
                //iOS
                shadowColor: '#000',
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                //Android
                shadowOpacity: 0.22,
                shadowRadius: 2.22,
                elevation: 3,
            },
            fabs: {
                //iOS
                shadowColor: '#000',
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.18,
                shadowRadius: 1.0,
                //Android
                elevation: 1,
            },
        },
    },
    outline: {
        borderColor: 'white',
        borderWidth: 1,
    },
};
